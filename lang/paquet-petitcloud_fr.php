<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// H
	'petitcloud_description' => 'Squelette d\'intranet',
	'petitcloud_nom' => 'Petit cloud',
	'petitcloud_slogan' => 'Un petit intranet pour les copains',
);
