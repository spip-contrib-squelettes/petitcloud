<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

// 4
'404_sorry' => 'Désolé, la page que vous cherchez n\'existe plus.',



//  A
'accueil_site' => 'Accueil',
'articles_maj_recents' => 'Articles mis à jour récemment',


// E
'effacer' => 'effacer',
'espace_prive' => 'Espace privé',

// H
'homepage_perso' => 'Espace personnel',
'homepage' => 'Accueil',
'haut_page' => 'haut de page',

// I
'identifiant' => 'Identifiant',

// L
'login_identification' => 'Se connecter',

// M
'mis_a_jour' => 'Mis à jour :',

// N
'nom' => 'Nom',

// R
'recherche_intranet' => 'Rechercher ...',
'recherche_rubrique' => 'Résultats sur les rubriques',
'recherche_article' => 'Résultats sur les articles',
'recherche_image' => 'Résultats sur les images',
'recherche_document' => 'Résultats sur les documents',
'recherche_no_match' => 'Désolé, votre recherche n\'a retourné aucun résultat.',
'recherche_sur' =>  'Recherche sur ',



// P
'publie_le' => 'Publié le :',




);
