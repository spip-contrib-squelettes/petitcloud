# Petit Cloud

![petitcloud](./img/petitcloud-xx.svg)

Petit cloud est un squelette spécialisé pour créer de petits extranets de façon très simple.

## Documentation

https://contrib.spip.net/5359
