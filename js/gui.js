/********************************
 *  GUI: interface utilisateur
 *
 ********************************/
 

$(document).ready(function(){


	// Lien en spip_out s'ouvre ds une nouvelle fenetre
	$('a.spip_out,a[rel*=external]').attr('target','_blank').attr('rel','external noopener noreferrer');


	// scroll doux
	$('.smooth-scroll').click (function() {
		var target = $(this).attr('href');
		var hash = "#" + target.substring(target.indexOf('#')+1);
		$('html, body').animate({ scrollTop:$(hash).offset().top 	}, 600);
		return false;
	});


});


// WOW: animation in scroll
// https://wowjs.uk/docs.html
new WOW().init();